# Community

[![Build Status](https://github-ci.bluerain.io/api/badges/elixirchina/community/status.svg)](https://github-ci.bluerain.io/elixirchina/community)
[![GitHub issues](https://img.shields.io/github/issues/elixirchina/community)](https://github.com/elixirchina/community/issues)
[![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)

基于 Phoenix LiveView 的论坛程序。
